<?php
/**
 * @file
 * Examples of availables hook available in this hook.
 */

/**
 * Implements hook_menu_export().
 */
function hook_menu_export($menu_name) {
  $export = new stdClass();
  $export->element = "toto";
  $export->hello = array(
    "en" => "world",
    'fr' => "monde",
    "multi" => array("yeahhhh", "awesome"),
  );
  $export->foo = "bar";

  return array("module_name" => $export);
}

/**
 * Implements hook_menu_export_import().
 */
function hook_menu_export_import($menu_name, $data) {
  // Do something.
}
